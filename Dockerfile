FROM postgres:9.6

MAINTAINER Curtis Maloney <curtis@codealchemy.com.au>

RUN apt update -y && apt install -y postgis
